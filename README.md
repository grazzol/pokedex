# <font color='red'> Projet Pokemon Aurian Baudet et Soleymane Latrèche </font>

## Projet :

- Ce projet pokémon contient toutes les fonctionnalitées demandées
- Il a été fait via git 
- Soleymane ayant eu un problème avec git le code a été partagé et Aurian s'occupait de poster sur git après revue du code
  
## Lancer le projet :

Installation du projet :

```
// Dans un terminal git :

git clone https://gitlab.com/grazzol/pokedex.git

```

Installation des packages :

```
npm i

```

Dans deux terminaux distinct lancez ces deux commandes :

```
// Lancement du localhost sur le port 3000

npm run dev 

// Lancement de la base de donnée JSON sur le port 3001

npm run server

```